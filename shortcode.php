<?php

add_shortcode('get_n64_price', 'n64price_shortcode');
function n64price_shortcode ($atts){
	global $wpdb;
	$atts = array_change_key_case((array)$atts, CASE_LOWER);
	$id = $atts['id'];
	$output = '';
	$gamedata = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."price_queries WHERE id = $id;")[0];
	$output .= "<div class='game-meta-text'><dt>Weekly sales: </dt><dd>".number_format($gamedata->sevendaysales)."</dd></div>";
	$output .= "<div class='game-meta-text' itemprop=\"offers\" itemscope itemtype=\"http://schema.org/Offer\"><dt>Average price: </dt><dd>$<span itemprop=\"price\">".($gamedata->sixmonthprice/100)."</span></dd><meta itemprop=\"priceCurrency\" content=\"USD\" /></div>";
	//$output .= json_encode($gamedata);
	return($output);
}

add_shortcode('n64price_all', 'n64price_all_func');
function n64price_all_func(){
	global $wpdb;
	$output = '';
	
	// Get the top changers for this week
	
	$topsales = $wpdb->get_results("SELECT id, display, (sevendayprice/sixmonthprice) as pricechange FROM ".$wpdb->prefix."price_queries WHERE sevendayprice > 0 AND age > 14 ORDER BY pricechange DESC");
	$topQuan = get_option("n64price_top_list",2);
	$output .= "<h2 id=\"recent_head\">".get_option("n64price_top_title",'Recent changes')."</h2>";
	$output .= "<p>".get_option("n64price_top_desc",'')."</p>";
	foreach ($topsales as $k => $s) {
		if($k>$topQuan-1 && $k<count($topsales)-$topQuan){
			continue;
		} else if ($s->pricechange>1) {
			$class='n64price_topitem_plus';
		} else {
			$class='n64price_topitem_minus';
		}
		$output .= "<div class=\"$class\" target=\"$s->id\" game=\"$s->id\" data-nonce=\"".wp_create_nonce("gameid".$s->id)."\">";
			$output .= "<strong>".preg_replace('/\\\\/','',$s->display)."</strong> <span>".($s->pricechange>1?"+":"-").round((($s->pricechange>1)?($s->pricechange-1):(1-$s->pricechange))*100)."%</span>";
		$output .= "</div>";
	}
	
	// Get long-term changers over the past 6 months
	
	$toptrends =  $wpdb->get_results(" SELECT id, display, pearson, gradient, (pearson * gradient) as pricechange FROM ".$wpdb->prefix."price_queries WHERE age > 3 AND pearson > 0 ORDER BY pricechange DESC");
	$output .= "<h2 id=\"longterm_head\">".get_option("n64price_trends_title",'Long term trends')."</h2>";
	$output .= "<p>".get_option("n64price_trends_desc",'')."</p>";
	foreach ($toptrends as $k => $s) {
		if($k>$topQuan-1 && $k<count($toptrends)-$topQuan){
			continue;
		} else if ($s->gradient>=0) {
			$class='n64price_topitem_plus';
		} else {
			$class='n64price_topitem_minus';
		}
		$output .= "<div class=\"$class\" target=\"$s->id\" game=\"$s->id\" data-nonce=\"".wp_create_nonce("gameid".$s->id)."\">";
			$output .= "<strong>".preg_replace('/\\\\/','',$s->display)."</strong> <span>".($s->gradient<0?"-":"+")."\$".number_format(abs($s->gradient),2)." p/d (".round($s->pearson*100)."%)</span>";
		$output .= "</div>";
	}
	//echo "<pre>".var_export($toptrends,true).$wpdb->last_query."</pre>";
	// Get all games list
	
	
	$output .= "<h2 id=\"item_list\">".get_option("n64price_list_title",'Item list')."</h2>";
	$output .= "<p>".get_option("n64price_list_desc",'')."</p>";
	$order='sales DESC';
	$wherePlus='';
	if(isset($_GET['order'])){
		switch ($_GET['order']) {
			case 'name':
				$order='display ASC';
				break;
			case 'sales':
				break;
			case 'price':
				$order='sixmonthprice ASC';
				$wherePlus=' AND sevendaysales > 0';
				break;
			case 'change':
				$order='gradient ASC';
				$wherePlus=' AND pearson > 0';
				break;
			case 'update':
				$order='lastrun DESC';
				$wherePlus=' AND age > 0';
				break;
			default:
				break;
		}
	} else {
		$_GET['order']='sales';
	}
	$games = $wpdb->get_results("SELECT id, display, sevendaysales AS 'sales' FROM ".$wpdb->prefix."price_queries WHERE age > 0".$wherePlus." ORDER BY $order");
	$output .= "<div>";
		$output .= "<p>Sort by: ".
		($_GET['order']!='sales'?"<a class=\"sortlink\" href=\"".get_permalink()."#item_list\">Sales</a> ":"<strong>Sales</strong> ").
		($_GET['order']!='name'?"<a class=\"sortlink\" href=\"".get_permalink()."?order=name#item_list\">Name</a> ":"<strong>Name</strong> ").
		($_GET['order']!='price'?"<a class=\"sortlink\" href=\"".get_permalink()."?order=price#item_list\">Price</a> ":"<strong>Price</strong> ").
		($_GET['order']!='change'?"<a class=\"sortlink\" href=\"".get_permalink()."?order=change#item_list\">Price change</a> ":"<strong>Price change</strong> ").
		($_GET['order']!='update'?"<a class=\"sortlink\" href=\"".get_permalink()."?order=update#item_list\">Last updated</a>":"<strong>Last updated</strong>");
		 //echo $wpdb->last_query;
	foreach ($games as $game) {
		$output .= "<div class=\"n64price_gamitem\" id=\"n64price_t_$game->id\" game=\"$game->id\" data-nonce=\"".wp_create_nonce("gameid".$game->id)."\">";
			$output .= preg_replace('/\\\\/','',$game->display)." ($game->sales)";
		$output .= "</div>";
		$output .= "<div id=\"n64price_$game->id\" class=\"n64price_chartarea\">";
			$output .= '<p><a id="butan_'.$_REQUEST['game'].'" class="butan_more" rel="leanModal" name="popper_'.$game->id.'" href="#popper_'.$game->id.'">Enlarge</a> | <span class="butan_more" expander="more_price_info_'.$game->id.'">More info</span> | <span class="butan_top">Back to top</span></p>';
			$output .= "<div id=\"more_price_info_$game->id\" class=\"n64price_more_info\">";
				//$output .= "Pingas";
			$output .= "</div>";
		$output .= "</div>";
	}
	$output .= "</div>";
	$output .= '<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>';
	$output .= '<script type="text/javascript" src="'.plugins_url( '/n64price/leanmodal.js').'"></script>';
	return $output;
}

add_action("wp_ajax_get_price", "get_n64_price");
add_action("wp_ajax_nopriv_get_price", "get_n64_price");

function movingAverage ($array,$period) {
	$output = array();
	if(array_sum($array) == 0 || $period == 0){
		return $array;
	}
	foreach ($array as $k => $v) {
		$sum = $count = $l = $r = 0;
		for($i=1;$i<=$period;$i++){
			if(isset($array[$k-$i-$l])){
				while (isset($array[$k-$i-$l-1]) && $array[$k-$i-$l] == 0){
					$l++;
				}
				if($array[$k-$i-$l] >0){
					$sum +=$array[$k-$i-$l];
					$count++;
				}
			}
			if(isset($array[$k+$i+$r])){
				while (isset($array[$k+$i+$r+1]) && $array[$k+$i+$r] == 0 && $k+$i+$r < count($array)-1){
					$r++;
				}
				if($array[$k+$i+$r] >0){
					$sum +=$array[$k+$i+$r];
					$count++;
				}
			}
		}
		array_push($output,number_format(($sum+$v)/($count+($v?1:0)*1),2));
	}
	return $output;
}

function get_n64_price() {
	$colours = array("#88abf7","#f78888","#88f788","#db88f7","#8888f7");
	global $wpdb;
	if ( !wp_verify_nonce( $_REQUEST['nonce'], "gameid".$_REQUEST['game'])) {
		$return['response'] = "nonce";
		$return['nonce'] = "nonce";
		die(json_encode($return));
	}  
	
	
	$qr = $wpdb->get_row( "SELECT * FROM ".$wpdb->prefix."price_queries WHERE id = ".$_REQUEST['game'] );
	$numberOfVariants = count(explode('|',$qr->variants));
	$age = $wpdb->get_var( "SELECT COUNT(*) FROM ".$wpdb->prefix."price_data WHERE query = ".$_REQUEST['game'] );
	$displayArray = explode('|',$qr->variantsdisplay);
	$datasets = $labelset = $colorString = '';
	$dateFormat = "j M";
	$minmax = array();
	foreach ($displayArray as $d){
		$colorString = $colours[isset($counter)?$counter=$counter+1:$counter=0];
		if ($age <= 30) {
			$priceData = $wpdb->get_results("SELECT sales, price, date AS label FROM ".$wpdb->prefix."price_data WHERE query = ".$_REQUEST['game']." AND variant LIKE '$d' ORDER BY date ASC");
		} else if ($age <= 180) {
			$priceData = $wpdb->get_results("SELECT SUM(sales) AS sales, SUM(price*sales)/SUM(sales) AS price, week AS label FROM ".$wpdb->prefix."price_data WHERE query = ".$_REQUEST['game']." AND variant LIKE '$d' GROUP BY week ORDER BY date ASC");
		} else {
			$priceData = $wpdb->get_results("SELECT SUM(sales) AS sales, SUM(price*sales)/SUM(sales) AS price, CONCAT(YEAR(date),'-', MONTH(date),'-01') AS label FROM ".$wpdb->prefix."price_data WHERE query = ".$_REQUEST['game']." AND variant LIKE '$d' GROUP BY CONCAT(YEAR(date),'-', MONTH(date), '-01') ORDER BY date ASC");
			$dateFormat = "M Y";
		}
		
		$salesArray = array();
		$priceArray = array();
		$labelArray = array();
		$max = $min = 0;
		$numbers ='';
		foreach ($priceData as $p){
			$p->price /= 100;
			array_push($salesArray, $p->sales);
			array_push($priceArray, $p->price);
			array_push($labelArray, date($dateFormat, strtotime($p->label)));
			$max = ($max == 0 || $max < $p->price) ? $p->price : $max;
			$min = ($min == 0 || $min > $p->price) ? $p->price : $min;
			$numbers.=$p->price.", ";
		}
		array_push($minmax,(object) array('display'=>$d,'min'=>$min,'max'=>$max,'numbers'=>$numbers));
		//$priceArray = array_reverse($priceArray);
		$averageArray = movingAverage($priceArray,get_option("n64price_moving_average",0));
		$priceString = implode(', ',$priceArray);
		$averageString = implode(', ',$averageArray);
		$labelString = "\"".implode('", "',$labelArray)."\"";
		$datasets .= '{
				label: "'.$d.'",
				data: ['.$averageString.'],
				borderColor: \''.$colorString.'\',
				backgroundColor: \''.$colorString.'40'.'\',
				pointBorderColor: \''.$colorString.'\',
				pointBackgroundColor: \''.$colorString.'\',
			},';
	}
	//die(json_encode($datasets));
	//$return['response'] = $datasets;
	//die(json_encode($return));
	
	
	
	$return['chart'] = '
	<canvas id="chart'.$_REQUEST['game'].'" width="200" height="50"></canvas>
	<script>
	jQuery("a[rel*=leanModal]").leanModal();
	var ctx = document.getElementById("chart'.$_REQUEST['game'].'");
	var settings = {
		type: "line",
		data: {
			labels: ['.$labelString.',],
			datasets: ['.$datasets.'],
		},
		options: {
			responsive: true,
			tooltips: {
				mode: \'index\',
				intersect: false,
			},
			hover: {
				mode: \'index\',
				intersect: false
			},
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero:true,
						callback: function(value, index, values) {
							return "$" + value;
						}
					}
				}],
				xAxes: [{
				}]
			},
		}
	}
	var chart'.$_REQUEST['game'].' = new Chart(document.getElementById("chart'.$_REQUEST['game'].'"), settings);
	var chart'.$_REQUEST['game'].' = new Chart(document.getElementById("chart'.$_REQUEST['game'].'_2"), settings);
	 </script>
	 	<div id="popper_'.$_REQUEST['game'].'" class="leanmodal">
	 		<h2 style="text-align:center; font-weight:bold;">'.preg_replace('/\\\\/','',$wpdb->get_var("SELECT display FROM ".$wpdb->prefix."price_queries WHERE id = ".$_REQUEST['game'])).'</h2>
			<canvas id="chart'.$_REQUEST['game'].'_2" width="200" height="50"></canvas>
		</div>';
	$return['response'] = 'ok' ;
	$return['more_info'] = "<hr><p>
	Variants: ".preg_replace('/\|/',', ',$qr->variantsdisplay)."<br>
	30-day sales: $qr->sevendaysales<br>
	Data age: $qr->age days<br>
	Average price: $qr->sixmonthprice days<br>
	Price change per day: ".($qr->gradient<0?"-":"+")."\$".abs($qr->gradient)."<br>
	Price change accuracy: ".(round($qr->pearson*10000)/100)."%<br>";
	foreach($minmax as $m) {
		$return['more_info'] .= "Max ".$m->display.": $".number_format($m->max,2)."<br>";
		$return['more_info'] .= "Min ".$m->display.": $".number_format($m->min,2)."<br>";
		//$return['more_info'] .= "Numbers: $".$m->numbers."<br>";
	}
	$return['more_info'] .= "</p>";
	die(json_encode($return));
}