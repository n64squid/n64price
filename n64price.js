jQuery(document).ready( function() {
	var n64price_loading = false;
	jQuery('.n64price_topitem_plus,.n64price_topitem_minus,.n64price_gamitem').click(function() {
		if(jQuery(this).attr('target') != undefined){
			var target="#n64price_t_"+jQuery(this).attr('target');
			//alert(target);
			jQuery('html,body').delay(750).animate({scrollTop: jQuery(target).offset().top - jQuery(target).height()},'slow');
		}
		if (!n64price_loading && !jQuery("#n64price_"+ jQuery(this).attr("game")).is(":visible")) {
			jQuery("body, .n64price_gamitem").css('cursor','progress');
			nonce = jQuery(this).attr("data-nonce");
			game = jQuery(this).attr("game");
			n64price_loading = true;
			jQuery.ajax({
				type : "post",
				dataType : "json",
				url : my_ajax_object.ajax_url,
				data : {action: "get_price", nonce: nonce, game: game},
				timeout : 10000,
				success: function(response) {
					jQuery('#n64price_'+game).prepend(response.chart);
					jQuery('#more_price_info_'+game).html(response.more_info);
					jQuery("#n64price_"+game).slideDown();
					jQuery("body, .n64price_topitem_plus,.n64price_topitem_minus,.n64price_gamitem").removeAttr('style');
					n64price_loading = false;
				},
				error: function (xhr, textStatus, thrownError) {
					alert(textStatus+": "+thrownError);
					jQuery("body, .n64price_topitem_plus,.n64price_topitem_minus,.n64price_gamitem").removeAttr('style');
					n64price_loading = false;
				},
			});
		}
	});
	jQuery('.butan_top').click(function() {
		jQuery('html,body').animate({scrollTop: jQuery("#recent_head").offset().top},'slow');
	});
	jQuery('.butan_more').click(function() {
		jQuery('#'+jQuery(this).attr('expander')).slideToggle('slow');
	});
});
	