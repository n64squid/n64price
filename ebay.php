<?php
add_action("wp_ajax_n64price_check", "n64price_check_func");
add_action("wp_ajax_nopriv_n64price_check", "n64price_check_func");

function n64price_check_func() {
	header('Content-type: application/json');
	$curdate = date('Y-m-d', time() - (60*60*24));
	$avgprice = 0;
	global $wpdb;
	$result = array();
	$qr = $wpdb->get_row( "SELECT * FROM ".$wpdb->prefix."price_queries ORDER BY lastrun ASC" );
	
	$variants = explode('|',$qr->variants);
	$displays = explode('|',$qr->variantsdisplay);
	$wpdb->update($wpdb->prefix . 'price_queries', array('lastrun' => time()), array('id' => $qr->id));
	if ($wpdb->get_var( "SELECT COUNT(*) FROM ".$wpdb->prefix."price_data WHERE query LIKE ".$qr->id." AND `date` = '".$curdate."'") > 0) {
		die(json_encode((object) array(
			'result'=>'All queries completed for today',
			'name'=>$qr->display,
			'date'=>$curdate,
			'records' => $wpdb->get_var("SELECT COUNT(*) FROM ".$wpdb->prefix."price_data WHERE query LIKE ".$qr->id." AND `date` = '".$curdate."'"),
			'time'=>date('Y-m-d H:i:s'),
			'queryResults'=> (object)$qr,
		)));
	}
	
	
	
	set_time_limit (120);
	$executionStartTime = microtime(true);
	
	$i=0;
	$itemId = $updateArray = array();
	foreach ($variants as $k => $v) {
		$game = isset($qr->query)?$qr->query:'n64 ocarina';
		$search_query = $game.' '.$v.' -lot';
		
		
		error_reporting(E_ALL);  // Turn on all errors, warnings and notices for easier debugging
		$exchangeRates = json_decode(file_get_contents("http://api.fixer.io/".$curdate."?base=USD")); // From fixer.io
		$countries = (object) array(
			'USA' => (object) array(
				'globalId' => 'EBAY-US',
				'exchange' => (float) 1
			),
			'CanadaEn' => (object) array(
				'globalId' => 'EBAY-ENCA',
				'exchange' => $exchangeRates->rates->CAD
			),
			'CanadaFr' => (object) array(
				'globalId' => 'EBAY-FRCA',
				'exchange' => $exchangeRates->rates->CAD
			),
			'UK' => (object) array(
				'globalId' => 'EBAY-GB',
				'exchange' => $exchangeRates->rates->GBP
			),
			'Ireland' => (object) array(
				'globalId' => 'EBAY-IE',
				'exchange' => $exchangeRates->rates->EUR
			),
			'Australia' => (object) array(
				'globalId' => 'EBAY-AU',
				'exchange' => $exchangeRates->rates->AUD
			),
			'Germany' => (object) array(
				'globalId' => 'EBAY-DE',
				'exchange' => $exchangeRates->rates->EUR
			),
			'Spain' => (object) array(
				'globalId' => 'EBAY-ES',
				'exchange' => $exchangeRates->rates->EUR
			),
			'France' => (object) array(
				'globalId' => 'EBAY-FR',
				'exchange' => $exchangeRates->rates->EUR
			),
			'Italy' => (object) array(
				'globalId' => 'EBAY-IT',
				'exchange' => $exchangeRates->rates->EUR
			),
			// Excluding the following countries to save run time and API calls
			/*
			'Netherlands' => (object) array(
				'globalId' => 'EBAY-NL',
				'exchange' => $exchangeRates->rates->EUR
			),
			'BelgiumFr' => (object) array(
				'globalId' => 'EBAY-FRBE',
				'exchange' => $exchangeRates->rates->EUR
			),
			'BelgiumNl' => (object) array(
				'globalId' => 'EBAY-NLBE',
				'exchange' => $exchangeRates->rates->EUR
			),
			'Poland' => (object) array(
				'globalId' => 'EBAY-PL',
				'exchange' => $exchangeRates->rates->PLN
			),
			'Austria' => (object) array(
				'globalId' => 'EBAY-AT',
				'exchange' => $exchangeRates->rates->EUR
			),
			'Switzerland' => (object) array(
				'globalId' => 'EBAY-CH',
				'exchange' => $exchangeRates->rates->CHF
			),'Philippines' => (object) array(
				'globalId' => 'EBAY-PH',
				'exchange' => $exchangeRates->rates->PHP
			),
			'Singapore' => (object) array(
				'globalId' => 'EBAY-AU',
				'exchange' => $exchangeRates->rates->SGD
			),
			'HongKong' => (object) array(
				'globalId' => 'EBAY-HK',
				'exchange' => $exchangeRates->rates->HKD
			),
			'India' => (object) array(
				'globalId' => 'EBAY-IN',
				'exchange' => $exchangeRates->rates->INR
			),
			'Malaysia' => (object) array(
				'globalId' => 'EBAY-MY',
				'exchange' => $exchangeRates->rates->MYR
			),*/);
		// API request variables
		$endpoint = 'https://svcs.ebay.com/services/search/FindingService/v1';  // URL to call
		$version = '1.12.0';  // API version supported by your application
		include_once("appid.php");
		$safequery = urlencode(preg_replace('/\\\\/','',$search_query));  // Make the query URL-friendly
		$results = '';
		$prices = array();
		foreach ($countries as $c){
			// Construct the findItemsByKeywords HTTP GET call
			$apicall = "$endpoint?";
			$apicall .= "OPERATION-NAME=findCompletedItems";
			$apicall .= "&SERVICE-VERSION=$version";
			$apicall .= "&SECURITY-APPNAME=$appid";
			$apicall .= "&GLOBAL-ID=".$c->globalId;
			$apicall .= $qr->category? "&categoryId=".$qr->category: "";
			$apicall .= "&keywords=$safequery";
			$apicall .= "&paginationInput.entriesPerPage=300";
			//die($apicall);
			// Load the call and capture the document returned by eBay API
			$resp = simplexml_load_file($apicall);
			
			// Check to see if the request was successful, else print an error
			if ($resp->ack == "Success") {
				// If the response was loaded, parse it and build links
				foreach($resp->searchResult->item as $item) {
					$date = substr($item->listingInfo->endTime,0,10);
					if($item->sellingStatus->sellingState != 'EndedWithSales' || $date != date('Y-m-d', time() - 60 * 60 * 24)){
						continue;
					} else {
						foreach($itemId as $it) {
							if ($it == (string)$item->itemId){
								continue 2;
							}
						}
					}
					array_push($itemId,(string)$item->itemId);
					
					$pic   = $item->galleryURL;
					$link  = $item->viewItemURL;
					$title = $item->title;
					$price = round($item->sellingStatus->convertedCurrentPrice/$c->exchange*100)/100;
					$countr = $item->globalId;
					array_push($prices,$price);
					
					// For each SearchResultItem node, build a link and append it to $results
					$results .= "<tr><td><img src=\"$pic\"></td><td><a href=\"$link\">$title</a><br>\$$price $countr, $date</td></tr>";
				}
			}
			// If the response does not indicate 'Success,' print an error
			else {
			  die ("<h3>Oops! The request was not successful. Make sure you are using a valid AppID for the Production environment.</h3>".$resp->ack.'<br>'.$apicall.'<pre>'.var_dump($resp).'</pre>');
			}
		}
		
		$avg = count($prices)>0?round(array_sum($prices)/count($prices)*100):0;
		$executionEndTime = microtime(true);
		array_push($result,(object) array(
			'queryId' => $qr->id,
			'game' => preg_replace('/\\\\/','',$qr->display),
			'type' => $displays[$i],
			'averagePrice' => $avg/100,
			'sales' => count($prices),
			'date' => $curdate,
			'executeTime' => $executionEndTime-$executionStartTime,
		));
		array_push($updateArray,array('query' => $qr->id, 'variant' => $displays[$k], 'price' => $avg, 'sales' => count($prices), 'date' => $curdate, 
														  'week' => date('Y-m-d',floor((time()-(86400*$d))/604800-(4/7))*604800+259200)));
		//$wpdb->insert($wpdb->prefix . 'price_data', array('query' => $qr->id, 'variant' => $displays[$k], 'price' => $avg, 'sales' => count($prices), 'date' => $curdate, 
		//												  'week' => date('Y-m-d',floor((time()-(86400*$d))/604800-(3/7))*604800+259200)));
		
	}
	$salesthisweek = $wpdb->get_var( "SELECT SUM(sales) FROM ".$wpdb->prefix."price_data WHERE date BETWEEN adddate(now(),-31) and now() AND query LIKE ".$qr->id );
	// Get the price for the past week and six months before that
	$weekPrice=round($wpdb->get_var("SELECT SUM(sales*price)/SUM(sales) FROM ".$wpdb->prefix."price_data WHERE date BETWEEN adddate(now(),-7) and now() AND query LIKE ".$qr->id));
	$monthPrice=round($wpdb->get_var("SELECT SUM(sales*price)/SUM(sales) FROM ".$wpdb->prefix."price_data WHERE date BETWEEN adddate(now(),-180) and adddate(now(),-7) AND query LIKE ".$qr->id));
	
	array_push($result,(object) array(
		'weeks'=>$weekPrice,
		'month'=>$monthPrice,
	));
	
	// Do the correlation bits
	
	$variants = explode('|',$qr->variantsdisplay);
	$sum=$i=0;
	$gradientArray = $pearsonArray = array();
	foreach ($variants as $k=>$v) {
		$pairsum = $xsum = $ysum = $x2sum = $y2sum = $lowest = $highest = $n = $xssum = $yssum = 0;
		$data = $wpdb->get_results("SELECT date, price, variant FROM ".$wpdb->prefix."price_data WHERE query = $qr->id AND price != 0 AND variant LIKE '$v' ORDER BY date ASC");
		$i++;
		if(count($data)<5){
			array_push($result,(object)array('variant'=>$v,'error'=>'too few data points','count'=>count($data)));
			continue;
		}
		$avg = $wpdb->get_var("SELECT AVG(price) FROM ".$wpdb->prefix."price_data WHERE query = $qr->id AND price != 0 AND variant LIKE '$v' GROUP BY variant")/100;
		$firstday = strtotime($data[0]->date)/(24*3600);
		foreach ($data as $k => $d) {
			$y = $d->price/100;
			$lowest = ($lowest == 0 || $y < $lowest) ? $y : $lowest;
			$x = floor(strtotime($d->date)/(24*3600))-$firstday;
			$xsum += $x;
			$ysum += $y;
			$x2sum += $x*$x;
			$y2sum += $y*$y;
			$pairsum += $x*$y;
			$xssum += pow($avg-$x,2);
			$yssum += pow($avg-$y,2);
			//array_push($result,(object)array('y'=>$y,'avg' => $avg));
			$n++;
		}
		$t = (($n*$pairsum)-($xsum*$ysum));
		$b = sqrt((($n*$x2sum)-($xsum*$xsum))*(($n*$y2sum)-($ysum*$ysum)));
		$c = ($t / $b);
		$f = $g*$c;
		$sx = sqrt(pow($n,-1)*$xssum);
		$sy = sqrt(pow($n,-1)*$yssum);
		$g = $c*($sy/$sx);
		array_push($result,(object)array('count'=>$i,'variant'=>$v,'xsum'=>$xsum,'ysum'=>$ysum,'x2sum'=>$x2sum,'y2sum'=>$y2sum,'pairsum'=>$pairsum,'t'=>$t,'b'=>$b,'c'=>$c,'f'=>$f,'lowest'=>$lowest,'ssum'=>$ssum,'s(x)'=>$sx,'s(y)'=>$sy,'g'=>$g));
		array_push($pearsonArray,abs($c));
		array_push($gradientArray,$g);
	}
	$k++;
	$age = round($wpdb->get_var("SELECT count(*) FROM ".$wpdb->prefix."price_data WHERE query = $qr->id AND price > 0")/$i);
	$pearson = $age>3 ? array_sum($pearsonArray) / count($pearsonArray) : 0;
	$gradient = $age>3 ? array_sum($gradientArray) / count($gradientArray) : 0;
	array_push($result,(object)array(
		'id'=>$qr->id,
		'age' => $age,
		'pearsonArray'=>$pearsonArray,
		'gradientArray'=>$gradientArray,
		'$k'=>$k
	),(object)array(
		'lastrun' => time(),
		'sevendaysales' => $salesthisweek,
		'sevendayprice' => $weekPrice,
		'sixmonthprice' => $monthPrice,
		'pearson' => $pearson,
		'gradient' => $gradient,
		'age' => (object)array('age'=>$age,'i'=>$i,'count'=>$wpdb->get_var("SELECT count(*) FROM ".$wpdb->prefix."price_data WHERE query = $qr->id"))
	),(object)array(
		'pearson' => (object)$pearsonArray,
		'gradient' => (object)$gradientArray,
	),(object)$updateArray);
	
	foreach ($updateArray as $u) {
		$wpdb->insert($wpdb->prefix . 'price_data', $u);
	}
	$wpdb->update($wpdb->prefix . 'price_queries', 
		array(
			'lastrun' => time(),
			'sevendaysales' => $salesthisweek,
			'sevendayprice' => $weekPrice,
			'sixmonthprice' => $monthPrice,
			'pearson' => $pearson,
			'gradient' => $gradient,
			'age' => $age
		), 
		array('id' => $qr->id)
	);
	// Get the correlation coefficient
	die(json_encode($result));

	exit;
}