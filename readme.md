# Price checker

![Price checker image](price-checker.jpg)

This is a plugin for wordpress which goes to ebay and uses eBay's API to get prices of sold products on a daily basis to get their prices. To get it to work, upload the files into a folder called 'n64price' in your plugins folder. Then go to your Wordpress dashboard and install like any other plugin.

You will then have to go to the "Price checker" menu on the left of your admin panel, where you can set your eBay queries and plugin options. You might need to scour eBay to get the correct category ids and product names.

Then use this shortcode to insert into a page:

    [n64price_all]

The plugin is now deprecated since 2020 since eBay has changed its API to paid-only, and I'm not going to pay to get access to this. You're free to contribute if you want though.

You can see a sample of what this code would output here:

[N64 price checker](https://n64squid.com/price-checker/)