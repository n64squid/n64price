<?php
// Voting

add_action('admin_head', 'n64price_custom_fonts');
function n64price_custom_fonts() {
  echo '<style>
    table {
   		border-collapse: collapse;
	}
	
	.users_n64price table, .users_n64price td {
		border: 1px solid black;
		padding:5px;
		max-width:100%;
	}
	.users_n64price td .flairinput, .loading {
		display:none;
	}
	.usertable {
		max-width:100%;
	}
	.usertablehead {
		background-color: #d8d8d8;
		font-weight:bold;
	}
	.dashicons-n64price {
		background-image: url("'.plugins_url().'/n64price/images/cake-gray.png");
		background-repeat: no-repeat;
		background-position: center; 
	}
	.dashicons-n64price-edit {
		background-image: url("'.plugins_url().'/n64price/images/cake-pencil-gray.png");
		background-repeat: no-repeat;
		background-position: center; 
	}
	.n64price_flex_wrapper {
		display: flex;
		align-items: stretch !important;
	}
	.n64price_form {
		width:40%;
		margin-bottom:20px;
	}
	.n64price_testing {
		width:55%;
		margin-left:5%;
	}
	.n64price_testing .tablewrapper {
		height:400px;
		overflow-y: scroll;
	}
	.n64price_form input {
		width:100%;
		border:1px solid #BEBEBE;
		padding: 7px;
	}
	.n64price-category, .price-query-id {
		cursor:pointer;
		user-select: none;
	}
	</style>
	';
}
add_action('admin_head','n64pricescripts');
function n64pricescripts() {
	echo '<script type="text/javascript">'.PHP_EOL;
	echo 'ajaxurl = "' . admin_url('admin-ajax.php') . '";'.PHP_EOL;
	echo '</script>';
}
add_action( 'wp_enqueue_scripts', 'n64price_css' );