<?php
/**
 * Plugin Name: Video game price checker
 * Plugin URI: https://n64squid.com
 * Description: This plugin checks for prices of games online based on a set of queries (requires cron job access)
 * Version: 0.1
 * Author: N64 Squid
 * Author URI: https://n64squid.com
 * License: GPL2
 */
 
// function to create the DB / Options / Defaults					
function n64price_install() {
   	global $wpdb;
  	$query_table = $wpdb->prefix . 'price_queries';
  	$query_data = $wpdb->prefix . 'price_data';
	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
	if($wpdb->get_var("show tables like '$query_table'") != $query_table) {
		$sql = "CREATE TABLE " . $query_table . " (
		`id` mediumint UNSIGNED NOT NULL AUTO_INCREMENT,
		`query` varchar(255) NOT NULL,
		`display` varchar(255) NOT NULL,
		`variants` varchar(1023) NOT NULL,
		`variantsdisplay` varchar(1023) NOT NULL,
		`category` mediumint UNSIGNED NOT NULL,
		`sevendaysales` mediumint UNSIGNED NOT NULL,
		`sevendayprice` mediumint UNSIGNED NOT NULL,
		`sixmonthprice` mediumint UNSIGNED NOT NULL,
		`pearson` float NOT NULL,
		`gradient` float NOT NULL,
		`age` mediumint UNSIGNED NOT NULL,
		`lastrun` int NOT NULL,
		UNIQUE KEY id (id)
		) ".$wpdb->get_charset_collate().";";
		dbDelta($sql);
	}
	if($wpdb->get_var("show tables like '$query_data'") != $query_data) {
		$sql = "CREATE TABLE " . $query_data . " (
		`id` mediumint UNSIGNED NOT NULL AUTO_INCREMENT,
		`query` mediumint UNSIGNED NOT NULL,
		`variant` varchar(30) NOT NULL,
		`price` int UNSIGNED NOT NULL,
		`sales` int UNSIGNED NOT NULL,
		`date` date NOT NULL,
		`week` date NOT NULL,
		UNIQUE KEY id (id)
		) ".$wpdb->get_charset_collate().";";
		dbDelta($sql);
	}
	set_n64price_settings();
}
register_activation_hook(__FILE__,'n64price_install');


include ("admin/adminpanel.php");
include ("css.php");
include ("ebay.php");
include ("shortcode.php");
include ("functions.php");