<?php
global $wpdb;
echo '<div class="users_n64price">';
	echo '
	<div class="n64price_flex_wrapper">
		<form class="n64price_form" action="' . $_SERVER['REQUEST_URI'] . '" method="post">
			<h2 id="query_h2">New query</h2>
			<b>ID:</b> <i>(only for editing)</i><br>
			<input name="id" id="price-id"><br>
			<b>Query:</b><br>
			<input name="query" id="price-query"><br>
			<b>Display:</b><br>
			<input name="display" id="price-display"><br>
			<b>Category:</b> <i><span id="js_games" class="n64price-category">// Games: 139973</span> // <span id="js_consoles" class="n64price-category">Consoles 139971</span> // <span id="js_accessories" class="n64price-category">Accesories 54968</span></i><br>
			<input name="category" id="price-category"><br>
			<b>Variants:</b> <i>(vertical bar delimited)</i><br>
			<input name="variants" id="price-variants"><br>
			<b>Variants Display:</b> <i>(vertical bar delimited)</i><br>
			<input name="variantsdisplay" id="price-variants-display"><br>
			&nbsp;<br>
			<input name="submit" id="submit" class="button button-primary" value="Create query" type="submit">
		</form>
		';
		
	
	//echo '('.$_POST['id'].')';
	$lastid = '';
	if(isset($_POST['submit']) && $_POST['submit'] == 'Create query'){
		if($_POST['id'] == ''){
			$wpdb->insert($wpdb->prefix . 'price_queries', array('query' => $_POST['query'], 'display' => $_POST['display'], 'variants' => $_POST['variants'], 'variantsdisplay' => $_POST['variantsdisplay'], 'category' => $_POST['category'], 'lastrun' => time()));
			$lastid = $wpdb->insert_id;
			$_POST['id'] = $lastid;
		} else {
			$wpdb->update($wpdb->prefix . 'price_queries', array('query' => $_POST['query'], 'display' => $_POST['display'], 'variants' => $_POST['variants'], 'variantsdisplay' => $_POST['variantsdisplay'], 'category' => $_POST['category']), array('id' => $_POST['id']));
			$lastid = $_POST['id'];
		}
	} else if(isset($_POST['submit']) && $_POST['submit'] == 'Delete'){
		foreach ($_POST['deleteBox'] as $d){
			$wpdb->delete($wpdb->prefix .'price_queries', array('id' => $d));
		}
	}
	echo '<div class="n64price_testing">';
		if((isset($_POST['submit']) && $_POST['submit'] == 'Test') || $lastid != ''){
			include ("output.php");
		}
	echo '</div>';
	echo '</div>';
	$q= "SELECT * FROM ".$wpdb->prefix."_price_queries";
	$queries = $wpdb->get_results( "SELECT * FROM ".$wpdb->prefix."price_queries ORDER BY display ASC");
	//var_dump($queries);
	
	// Array of WP_User objects.
	echo '<h2>Current queries</h2>';
	echo '<form action="' . $_SERVER['REQUEST_URI'] . '" method="post">';
		echo "<table class=\"usertable\">";
			echo "<tr class=\"usertablehead\">";
				echo '<td></td>';
				echo '<td>ID</td>';
				echo '<td>Query</td>';
				echo '<td>Display</td>';
				echo '<td>Variants</td>';
				echo '<td>Variants Display</td>';
				echo '<td>Category</td>';
				echo '<td>Last Run</td>';
				echo '<td>Test</td>';
			echo "</tr>";
			foreach ( $queries as $query ) {
				echo "<tr>";
					echo '<td><input name="deleteBox[]" value="'.$query->id.'" type="checkbox"></td>';
					echo '<td class="price-query-id">' . esc_html($query->id) . '</td>';
					echo '<td class="price-query-query">' . esc_html(preg_replace('/\\\\/','',$query->query)) . '</td>';
					echo '<td class="price-query-display">' . esc_html(preg_replace('/\\\\/','',$query->display)) . '</td>';
					echo '<td class="price-query-variants">' . esc_html($query->variants) . '</td>';
					echo '<td class="price-query-variantsDisplay">' . esc_html($query->variantsdisplay) . '</td>';
					echo '<td class="price-query-category">' . esc_html($query->category) . '</td>';
					echo '<td class="price-query-lastrun">' . n64price_time_elapsed_string(date('Y-m-d H:i:s',$query->lastrun)) . '</td>';
					echo '<td class="price-query-test">
								<form action="' . $_SERVER['REQUEST_URI'] . '" method="post">
									<input name="submit" class="button button-primary" value="Test" type="submit">
									<input name="id" value="' . esc_html($query->id) . '" type="hidden">
								</form>
						  </td>';
				echo "</tr>";
			}
		echo "</table><br>";
		echo '<input name="submit" id="delete" class="button button-primary" value="Delete" type="submit">';
	echo "</form>";
echo '</div>';
?>