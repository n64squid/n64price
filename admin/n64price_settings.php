<?php
//////////////////////
// Declare settings //
//////////////////////

function set_n64price_settings() { 
	// On plugin activation
	add_option("n64price_moving_average", 2);
	add_option("n64price_top_list", 2);
	add_option("n64price_top_title", 'Recent changes');
	add_option("n64price_top_desc", '');
	add_option("n64price_trends_title", 'Long term trends');
	add_option("n64price_trends_desc", '');
	add_option("n64price_list_title", 'Item list');
	add_option("n64price_list_desc", '');
}

function n64price_theme_options() {
    // Assign the sections
    add_settings_section(
        'n64price_options_section',  // ID
        'N64 Price Options',         // Title
        'n64price_options_callback', // Callback function
        'n64price_options'           // Page
    );
    // Assign idividual settings
	
	// General settings
	$settingname = 'n64price_moving_average';
    add_settings_field( 
        $settingname,
        'N64prices moving average',
        'n64price_settings_num',
        'n64price_options', 
        'n64price_options_section',
        array(
            'How many adjacent values to include in each stat.',
			$settingname
        )
    );
	$settingname = 'n64price_top_list';
    add_settings_field( 
        $settingname,
        'Top list amount',
        'n64price_settings_num',
        'n64price_options', 
        'n64price_options_section',
        array(
            'The amount of items to show in each \'top\' section. Double it to consider the totals.',
			$settingname
        )
    );
	$settingname = 'n64price_top_title';
    add_settings_field( 
        $settingname,
        'Top changers title',
        'n64price_settings_text',
        'n64price_options', 
        'n64price_options_section',
        array(
            'The H2 header for the \'top changes\' section.',
			$settingname
        )
    );
	$settingname = 'n64price_top_desc';
    add_settings_field( 
        $settingname,
        'Top changers description',
        'n64price_settings_textarea',
        'n64price_options', 
        'n64price_options_section',
        array(
            'The description for the \'top changes\' section.',
			$settingname
        )
    );
	$settingname = 'n64price_trends_title';
    add_settings_field( 
        $settingname,
        'Top trends title',
        'n64price_settings_text',
        'n64price_options', 
        'n64price_options_section',
        array(
            'The H2 header for the \'top trends\' section.',
			$settingname
        )
    );
	$settingname = 'n64price_trends_desc';
    add_settings_field( 
        $settingname,
        'Top trends description',
        'n64price_settings_textarea',
        'n64price_options', 
        'n64price_options_section',
        array(
            'The description for the \'top trends\' section.',
			$settingname
        )
    );
	$settingname = 'n64price_list_title';
    add_settings_field( 
        $settingname,
        'List Title',
        'n64price_settings_text',
        'n64price_options', 
        'n64price_options_section',
        array(
            'The H2 header for the list section.',
			$settingname
        )
    );
	$settingname = 'n64price_list_desc';
    add_settings_field( 
        $settingname,
        'List description',
        'n64price_settings_textarea',
        'n64price_options', 
        'n64price_options_section',
        array(
            'The description for the list section.',
			$settingname
        )
    );
     
    // Finally, we register the fields with WordPress
	
	register_setting('n64price_options','n64price_moving_average');
	register_setting('n64price_options','n64price_top_list');
	register_setting('n64price_options','n64price_top_title');
	register_setting('n64price_options','n64price_top_desc');
	register_setting('n64price_options','n64price_trends_title');
	register_setting('n64price_options','n64price_trends_desc');
	register_setting('n64price_options','n64price_list_title');
	register_setting('n64price_options','n64price_list_desc');
	
} // end n64price_theme_options
add_action('admin_init', 'n64price_theme_options');
 
/* ------------------------------------------------------------------------ *
 * Section Callbacks
 * ------------------------------------------------------------------------ */
 
function n64price_options_callback() {
    echo '<p>General settings for the N64 Price plugin.</p>';
}  
 
/* ------------------------------------------------------------------------ *
 * Field Callbacks
 * ------------------------------------------------------------------------ */
 
function n64price_settings_check($args) {
    $html = '<input type="checkbox" id="'.$args[1].'" name="'.$args[1].'" '.checked(get_option($args[1]),'on', false).' />'; 
    $html .= '<label for="'.$args[1].'"> '  . $args[0] . '</label>'; 
    echo $html.false;
} 
function n64price_settings_text($args) {
    $html = '<input type="text" id="'.$args[1].'" name="'.$args[1].'" value="'.get_option($args[1]).'" />'; 
    $html .= '<label for="'.$args[1].'"> '  . $args[0] . '</label>'; 
    echo $html.false;
}
function n64price_settings_num($args) {
    $html = '<input type="number" step="any" id="'.$args[1].'" name="'.$args[1].'" value="' . get_option($args[1]) . '" /><br>'; 
    $html .= '<label for="'.$args[1].'"> '  . $args[0] . '</label>'; 
    echo $html;
}
function n64price_settings_textarea($args) {
    $html = $args[0].'<br>';
    $html .= '<textarea id="'.$args[1].'" name="'.$args[1].'">' . get_option($args[1]) . '</textarea>';
    echo $html;
}
function n64price_settings_curve($args) {
	$html = '<input type="radio" name="'.$args[1].'" value="none" '.checked(get_option($args[1]),'none', false).'>None<br>';
	$html .='<input type="radio" name="'.$args[1].'" value="line" '.checked(get_option($args[1]),'line', false).'>Linear<br>'; 
	$html .='<input type="radio" name="'.$args[1].'" value="quad" '.checked(get_option($args[1]),'quad', false).'>Quadratic<br>'; 
	$html .='<input type="radio" name="'.$args[1].'" value="sqrt" '.checked(get_option($args[1]),'sqrt', false).'>Square root<br>'; 
    $html .= $args[0]; 
    echo $html;
}
?>