jQuery(document).ready( function() {
	jQuery(".price-query-id").click( function() {
		jQuery("html, body").animate({ scrollTop: 0 }, "slow");
		jQuery("#price-id").val(jQuery(this).html());
		jQuery("#price-query").val(jQuery(this).siblings(".price-query-query").html());
		jQuery("#price-display").val(jQuery(this).siblings(".price-query-display").html());
		jQuery("#price-variants").val(jQuery(this).siblings(".price-query-variants").html());
		jQuery("#price-category").val(jQuery(this).siblings(".price-query-category").html());
		jQuery("#price-variants-display").val(jQuery(this).siblings(".price-query-variantsDisplay").html());
	});
	jQuery(".n64price-category").click( function() {
			jQuery("#price-variants").val('(box,boxed,cib,complete,completo,complete,boite,caja)| ');
		if (jQuery(this).attr('id') == 'js_games') {
			jQuery("#price-variants-display").val('Boxed|Cartridge');
			jQuery("#price-category").val(139973);
		} else if (jQuery(this).attr('id') == 'js_consoles') {
			jQuery("#price-variants-display").val('All');
			jQuery("#price-category").val(139971);
		} else if (jQuery(this).attr('id') == 'js_accessories') {
			jQuery("#price-category").val(54968);
			jQuery("#price-variants-display").val('Boxed|Loose');
		}
	});
});