<?php
//////////////////
// Menu Options //
//////////////////


function n64price_css() {
    wp_enqueue_style( 'n64price-css', plugins_url()."/n64price/n64price.css" );
	wp_enqueue_script('n64price-js',plugins_url( '/n64price/n64price.js'),array( 'jquery' ));
}
function n64price_plugin_menu() {
	$menu = add_menu_page('n64price', 'Price checker', 'administrator', 'n64price', 'n64price_settings', 'dashicons-n64squid-money', 66);
	//add_submenu_page( 'n64price', 'Queue', 'n64price Queue', 'manage_options', 'admin.php?page=n64price&tab=n64price_queue');
	add_action( 'admin_print_styles-' . $menu, 'n64price_css' );
}

function n64price_time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);
    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;
    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }
    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}
// Menu html

add_action('admin_menu', 'n64price_plugin_menu');
function n64price_settings() {
	echo '<div class="wrap">
	<div id="icon-themes" class="icon32"></div>
	<h2>Price Checker</h2>';
	settings_errors();
	$active_tab = isset( $_GET[ 'tab' ] ) ? $_GET[ 'tab' ] : 'n64price_queries';
	$navtabs = '<h2 class="nav-tab-wrapper"><a href="?page=n64price&tab=n64price_queries" class="nav-tab ';
	$navtabs .= ($active_tab == 'n64price_queries') ? 'nav-tab-active' : '';
	$navtabs .= '">Queries</a><a href="?page=n64price&tab=n64price_options" class="nav-tab ';
	$navtabs .= ($active_tab == 'n64price_options') ? 'nav-tab-active' : '';
	$navtabs .='">Options</a></h2>';
	echo $navtabs;
	echo '</div>';
	
	if($active_tab == "n64price_options"){
		echo '<form method="post" action="options.php">';
		settings_fields('n64price_options');
		do_settings_sections('n64price_options');
		submit_button();
		echo '</form>';
	} else {
		include ('query.php');
	}
} 


// Hide admin panel for all but admins, and redirect 
//add_action('after_setup_theme', 'remove_admin_bar');
add_action( 'admin_init', 'n64price_redirect_non_admin_users' );
function n64price_redirect_non_admin_users() {
	if ( !current_user_can('administrator') && !preg_match("/\/wp-admin\/admin-ajax\.php/",$_SERVER['PHP_SELF']) && !preg_match("/\/wp-admin\/admin-post\.php/",$_SERVER['PHP_SELF'])) {
		//wp_redirect( home_url() );
		//exit;
	}
}

function n64price_admin_css() {
	wp_enqueue_script('n64price-admin-js',plugins_url( '/n64price/admin/settings.js'),array( 'jquery' ));
}
add_action( 'admin_enqueue_scripts', 'n64price_admin_css' );
include ("n64price_settings.php");
?>